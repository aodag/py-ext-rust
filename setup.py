from setuptools import setup
from setuptools_rust import Binding, RustExtension

setup(
    name="py-ext-rust",
    rust_extensions=[
        RustExtension('hello_rust._helloworld', 'Cargo.toml', binding=Binding.PyO3),
    ],
    packages=["hello_rust"],
)